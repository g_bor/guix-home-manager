Using the Home Manager
======================

Installing a new Configuration
------------------------------

You should create a file that contains your home configuration. This file can
easily be kept in version control with git for instance. The file looks
like this:

```scheme
(use-modules (home))

(home "/data/alice" '())
```

Home Reference
--------------

**Scheme Procedure**: (use-home-modules module-name ...)

Import home modules with name _module-name_. For instance,

```scheme
(use-home-modules openbox hexchat)
```

is equivalent to:

```scheme
(use-modules (home openbox)
             (home hexchat))
```

**Scheme Procedure**: (home basedir inputs #:guix-symlink #:guix-config-symlink #:local-symlink #:cache-symlink))

Create a package that can then be installed with `guix package -f` from _inputs_
and essential configuration. _inputs_ must be a list of inputs, namely a list
of file-like objects. The resulting package is a union of all the inputs, with
the addition of a few essential symlinks.

By default, when no input is given, and no keyword argument is used, these
symlinks are created:

* `~/.guix-profile` → `${basedir}/.guix-profile`
* `~/.config/guix` → `${basedir}/.config/guix`
* `~/.local` → `${basedir}/.local`
* `~/.cache` → `${basedir}/.cache`

When the corresponding keyword argument is used, the associated path is used as
the target of the symbolic link.