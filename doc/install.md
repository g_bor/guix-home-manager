Installation Guide
==================

Installing the Channel
----------------------

This is a Guix channel. You will first need to
[install](https://www.gnu.org/software/guix/download/) Guix itself. Then, simply
create a new `~/.config/guix/guix/channels.scm` file with this content:

```scheme
(cons* (channel
        (name 'guix-home-manager)
        (url "https://framagit.org/tyreunom/guix-home-manager.git")
       %default-channels)
```

Then run `guix pull` to pull the new channel.

If the file already exist, simply add the `(channel)` form above to the list
of channels in the file. Make sure you use `(cons*)` (with a star) and not
`(cons)` since the later can only take one channel and %default-channels as
argument, whereas the first can take as many channels as you'd like before
%default-channels.

Usage
-----

### Making some room in your home directory

It is recommended to use this in a new install, when your home directory is
still pretty empty. Since your home directory will be made into a Guix profile,
you first want to create a new directory for your user data, for instance as
root:

```bash
mkdir -p /data/alice
chown alice: /data/alice
```

if your user is named alice. Then, move all your data over to that directory.
Do not transfer your configuration, it will be useless. That transfering of data
is the reason why it's simpler to start with an empty home: there is no data to
transfer ;)

Once that is done, some parts of your home directory will still have to be
read-write. This is mostly `~/.cache`, `~/.local` but also `~/.guix-profile` and
`~/.config/guix`. Inside your new data directory, create them like this, as your
regular user (alice in this example):

```bash
cd /data/alice
mkdir-p .local/share .cache .config
```

### Creating the first home generation

To create your first home configuration, you must create a configuration file.
For instance, create `/data/alice/.config/guix/home.scm`:

```scheme
(use-modules (home))

(home "/data/alice"
  '())
```

This will generate a completely empty home, except for essential configurations,
i. e. writable XDG directories as well as essential guix symlinks.

See the documentation to learn more about the configuration system.

To build your first generation of your home environment, run as your regular
user:

```bash
guix package -p /var/guix/profiles/per-user/alice/home \
  -f /data/alice/.config/guix/home.scm
```

Still as your regular user, copy your `~/.config/guix` to your data directory.
This will ensure you can still use Guix after you switch to the managed home
profile:

```bash
cp -ar ~/.config/guix /data/alice/.config/
```

Finaly, switch to the managed home profile as root:

```bash
mv /home/alice{,.bak} # keep a backup in case something goes wrong
ln -sv /var/guix/profiles/per-user/alice/home /home/alice
```

That's it!