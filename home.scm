;;; Guix Home Manager.
;;;
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home)
  #:use-module (guix build union)
  #:use-module (guix build utils)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (home build utils)
  #:export (home
            use-home-modules))

(define-syntax use-home-modules
  (syntax-rules ()
    ((_ modules ...)
     (use-modules (home modules) ...))))

(define* (home basedir inputs #:key
               (guix-symlink (string-append basedir "/.guix-profile"))
               (guix-config-symlink (string-append basedir "/.config/guix"))
               (local-symlink (string-append basedir "/.local"))
               (cache-symlink (string-append basedir "/.cache")))
  (define union
    (computed-file "home"
      #~(begin
          (use-modules (guix build union))
          (union-build #$output '#$inputs))
      #:options
      '(#:local-build? #t
        #:modules ((guix build union)))))
  (package
    (name "guix-home")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils) (home build utils))
       #:builder
       (begin
         (use-modules (guix build utils) (home build utils))
         (mkdir-p (home-file %outputs ".config"))
         ;; For guix
         (symlink ,guix-config-symlink (home-file %outputs ".config/guix"))
         (symlink ,guix-symlink (home-file %outputs ".guix-profile"))
         ;; symlink writeable directories
         (symlink ,local-symlink (home-file %outputs ".local"))
         (symlink ,cache-symlink (home-file %outputs ".cache"))
         ;; rest of the files
         (with-directory-excursion (assoc-ref %build-inputs "union")
         (for-each
           (lambda (f)
             (mkdir-p (home-file %outputs (dirname f)))
             (symlink (string-append (assoc-ref %build-inputs "union") "/" f)
                      (home-file %outputs f)))
           (find-files "." ".*"))))))
    (inputs
     `(("union" ,union)))
    (home-page "")
    (synopsis "")
    (description "")
    (license gpl3+)))