Guix Home Manager
=================

This is a [Guix](https://www.gnu.org/software/guix/) channel for user configuration
management. Guix itself comes with the possibility of managing system configuration
when installed as a standalone operating system. This project aims at going one
step further and managing your personal configuration entirely with Guix.

For this purpose, the channel extends Guix with more code that will allow you
to generate configuration files in a profile that will replace your home directory.
With Home Manager, your home directory is read-only, so software will not be
able to put any of their garbage in it, unless you specifically allow them to.

This is quite an extreme approach for configuration management, but it follows
the Guix spirit and provides strong guarantee that your configuration is exactly
as you want it to be.

Installing
----------

This is a Guix channel. You will first need to install
[Guix](https://www.gnu.org/software/guix/download/) itself. Then, simply create
a new `~/.config/guix/channels.scm` file with this content:

```scheme
(cons* (channel
        (name 'guix-home-manager)
        (url "https://framagit.org/tyreunom/guix-home-manager.git"))
       %default-channels)
```

Then run `guix pull` to pull the new channel.

Usage
-----

This is also [documented](doc/README.md) (and translated) in the documentation
section of this project:

* Il existe aussi une version [francophone](doc/README.fr.md) de la documentation.

### Making some room in your home directory

It is recommended to use this in a new install, when your home directory is
still pretty empty. Since your home directory will be made into a Guix profile,
you first want to create a new directory for your user data, for instance as
root:

```bash
mkdir -p /data/alice
chown alice: /data/alice
```

if your user is named alice. Then, move all your data over to that directory.
Do not transfer your configuration, it will be useless. That transfering of data
is the reason why it's simpler to start with an empty home: there is no data to
transfer ;)

Once that is done, some parts of your home directory will still have to be
read-write. This is mostly `~/.cache`, `~/.local` but also `~/.guix-profile` and
`~/.config/guix`. Inside your new data directory, create them like this, as your
regular user (alice in this example):

```bash
cd /data/alice
mkdir-p .local/share .cache .config
```

### Creating the first home generation

To create your first home configuration, you must create a configuration file.
For instance, create `/data/alice/.config/guix/home.scm`:

```scheme
(use-modules (home))

(home "/data/alice"
  '())
```

This will generate a completely empty home, except for essential configurations,
i. e. writable XDG directories as well as essential guix symlinks.

See the documentation to learn more about the configuration system.

To build your first generation of your home environment, run as your regular
user:

```bash
guix package -p /var/guix/profiles/per-user/alice/home \
  -f /data/alice/.config/guix/home.scm
```

Still as your regular user, copy your `~/.config/guix` to your data directory.
This will ensure you can still use Guix after you switch to the managed home
profile:

```bash
cp -ar ~/.config/guix /data/alice/.config/
```

Finaly, switch to the managed home profile as root:

```bash
mv /home/alice{,.bak} # keep a backup in case something goes wrong
ln -sv /var/guix/profiles/per-user/alice/home /home/alice
```

That's it!

Contributing
------------

Contributions are welcome! If there’s a configuration you would like to add, just
fork the repository and create a Merge Request when it is ready.

If you have a history of making quality contributions to GNU Guix and would like
commit access, just ask! Non trivial changes should still go through a simple
Merge Request and code review process, but this channel needs your help to take
over the world!

If you are less into code, we welcome contributions in the form of documentation,
translation, issues, reviews, tips and tricks. Do not hesitate to get in touch if
you have an idea or want to help in any way!